# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 17:47:48 2015

@author: ruhansa
"""
import json
# if you are using python 3, you should 
import urllib 
import urllib2
import sys
import requests
import langid
from subprocess import call
import os
# change the url according to your own koding username and query
#fp = open("queries.txt","rb")
#for line in fp:
IRModel=sys.argv[1]
#outfn = sys.argv[4]
lang= sys.argv[2]
temp = sys.argv[3].split(",")
ishash = sys.argv[4]
#strout= temp[1].find('#')
if ishash == "true":
  field = "tweet_hashtags"
else:   
  #lang= langid.classify(temp[1])
  field = "text_"+lang+"_case_fixed text_"+lang+""
#urlq = "q="+temp[1].rstrip()+"&rows=10000&fl=score,id&df="+field+"&wt=json"
urlbuild= { 'q' : ''+temp[1].rstrip()+'', 'rows':'10000','fl':'score,id','defType':'dismax','qf':''+field+'','wt':'json'}
urlq= urllib.urlencode(urlbuild)
inurl = "http://ukkk58981644.kushalb.koding.io:8983/solr/{0}/select?{1}".format(IRModel,urlq)
#inurl = "http://localhost:8983/solr/projb/query?id,score -d '{{ q: '{0}', limit: {1}'}} ".format(temp[1].rstrip(),10000)
print inurl
# change query id and IRModel name accordingly
qid = temp[0] 
outfn ="trec_eval/{0}{1}.txt".format(qid,IRModel)
outf = open(outfn, 'a+')
data = urllib.urlopen(inurl)
docs = json.load(data)['response']['docs']
# the ranking should start from 1 and increase
rank = 1
for doc in docs:
  outf.write(qid + ' ' + 'Q0' + ' ' + str(doc['id']) + ' ' + str(rank) + ' ' + str(doc['score']) + ' ' + IRModel + '\n')
  rank += 1
outf.close()
t= outfn.split("/")

#cmd= "-q -c -M1000 -m ndcg qrel.txt {0}".format(outfn)
os.chdir('trec_eval')
res= open("outputs/{0}/{1}.txt".format(IRModel,qid),"wb")
call(["./trec_eval","-q","-c","-M1000","-m","set_F.0.5","-m","map","-m","ndcg","-m","bpref","qrel.txt",""+t[1]+""],stdout=res)  
